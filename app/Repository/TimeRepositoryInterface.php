<?php

namespace App\Repository;

interface TimeRepositoryInterface
{
    /**
     * Get timer by id
     *
     * @param $id
     * @return mixed
     */
    public function get($id);

    /**
     * Update timer
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * Create new timer
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * Get running timers
     *
     * @return mixed
     */
    public function running();

    /**
     * Get stopped timers
     *
     * @return mixed
     */
    public function stopped();
}
