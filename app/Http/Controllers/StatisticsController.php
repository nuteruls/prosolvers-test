<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatisticsRequest;
use App\Services\PaginateTimer;
use App\Timer;

class StatisticsController extends Controller
{
    private $timer;
    private $request;

    /**
     * StatisticsController constructor.
     *
     * @param Timer $timer
     * @param StatisticsRequest $request
     */
    public function __construct(Timer $timer, StatisticsRequest $request)
    {
        $this->timer = $timer;
        $this->request = $request;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('statistics');
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $timers = app(PaginateTimer::class)->execute($this->request->validated());

        return $timers;
    }
}
