<?php

namespace App\Services;

use App\Timer;
use Carbon\Carbon;

class PaginateTimer
{
    private $timer;

    /**
     * PaginateTimer constructor.
     *
     * @param Timer $timer
     */
    public function __construct(Timer $timer)
    {
        $this->timer = $timer;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function execute($data)
    {
        $today = Carbon::today();

        $timers = $this->timer->whereNotNull('stopped_at');

        if (array_key_exists('year', $data)) {
            $timers->whereYear('created_at', $data['year']);
        }

        if (array_key_exists('month', $data)) {
            $timers->whereMonth('created_at', $data['month']);
        }

        if (array_key_exists('day', $data)) {
            $timers->whereDay('created_at', $data['day']);
        }

        if (!array_key_exists('day', $data) && !array_key_exists('year', $data) && !array_key_exists('month', $data)) {
            $timers->whereDate('created_at', $today);
        }

        $timers = $this->groupData($timers->get());
        return $timers;
    }

    /**
     * @param $timers
     * @return array
     */
    protected function groupData($timers)
    {
        $data = [];
        foreach ($timers as $timer) {
            $time = $timer->started_at->diffInSeconds($timer->stopped_at);

            $total = (isset($data[$timer->created_at->format('Y-m-d')]) ? $time + $data[$timer->created_at->format('Y-m-d')]['time'] : $time);
            $data[$timer->created_at->format('Y-m-d')] = [
                'time'  => $total
            ];

            $timer->time = $time;
        }

        return [
            'time'   => $data,
            'timers' => $timers
        ];
    }
}
