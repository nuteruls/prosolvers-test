<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    protected $fillable = [
        'title',
        'started_at',
        'stopped_at'
    ];

    protected $dates = [
        'started_at',
        'stopped_at'
    ];

}
